from aiogram.types import Message
from aiogram import html


def command_start_handler_text(message: Message):
    return (f"Привет, {html.bold(message.from_user.full_name)}!\n\n"
            f"В этом боте ты можешь настроить:\n- Аккаунты Facebook"
            f"\n- Ссылки на группы\n- Уведомления о новом сообщении"
            f"\n- Уведомления о постинге")


def txt_uploaded():
    return "Файл .txt успешно обработан!"


def not_txt():
    return "Это не файл .txt!"


def unknown_command():
    return "Такой команды нет!"


def account_added():
    return "Аккаунт добавлен!"


def task_added():
    return "Задача добавлена!"
