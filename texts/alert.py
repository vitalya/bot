def alerts_handler_text():
    return "Для управления уведомлениями о новом созданном посте:\n- /enable_posts_notification\n- /disable_posts_notification\n\nДля уведомлений о новых сообщениях:\n- /enable_messages_notification\n- /disable_messages_notification"


def enable_posts_notification_text():
    return "Уведомления о постах включены!"


def enable_messages_notifcation_text():
    return "Уведомления о сообщениях включены!"


def disable_posts_notifcation_text():
    return "Уведомления о постах выключены!"


def disable_messages_notifcation():
    return "Уведомления о сообщениях выключены!"
