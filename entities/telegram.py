from sqlalchemy import Column, Integer, String, Boolean, BigInteger

from entities.base import Base, engine, session


class Telegram(Base):
    __tablename__ = 'telegram_users'

    id = Column(Integer, primary_key=True)
    chat_id = Column(BigInteger)
    full_name = Column(String)
    message_alert_status = Column(Boolean, default=True)
    post_alert_status = Column(Boolean, default=True)


Base.metadata.create_all(engine)


def add_telegram(chat_id, full_name):
    if len(session.query(Telegram).all()) == 0:
        session.add(Telegram(chat_id=chat_id, full_name=full_name))
        session.commit()


def update_tg_posts(chat_id, status):
    tg: Telegram = session.query(Telegram).filter(Telegram.chat_id == chat_id)[0]
    tg.post_alert_status = status
    session.commit()


def update_tg_messages(chat_id, status):
    tg: Telegram = session.query(Telegram).filter(Telegram.chat_id == chat_id)[0]
    tg.message_alert_status = status
    session.commit()
