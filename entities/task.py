from sqlalchemy import Column, Integer, String, Boolean, ForeignKey
from sqlalchemy.orm import relationship

from entities.base import Base, engine, session


class Task(Base):
    __tablename__ = 'tasks'

    id = Column(Integer, primary_key=True)
    link = Column(String)
    is_exists = Column(Boolean, default=True)
    is_subscriber = Column(Boolean, default=False)
    is_posted = Column(Boolean, default=False)
    account_id = Column(Integer, ForeignKey('accounts.id'))
    account = relationship("Account", back_populates="tasks")


Base.metadata.create_all(engine)


def get_all_tasks():
    all_tasks = session.query(Task).all()
    return all_tasks


def add_task(link):
    session.add(Task(link=link))
    session.commit()


def clear_tasks():
    session.query(Task).delete()
    session.commit()
