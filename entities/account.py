from sqlalchemy import Column, Integer, String, Boolean
from sqlalchemy.orm import relationship

from entities.base import Base, engine, session


class Account(Base):
    __tablename__ = 'accounts'
    id = Column(Integer, primary_key=True)
    email = Column(String)
    password = Column(String)
    is_work = Column(Boolean, default=True)
    summary_usage_count = Column(Integer, default=0)

    tasks = relationship("Task", back_populates="account")


Base.metadata.create_all(engine)


def get_all_accounts():
    all_accounts = session.query(Account).all()
    return all_accounts


def add_account(email, password):
    session.add(Account(email=email, password=password))
    session.commit()


def clear_accounts():
    session.query(Account).delete()
    session.commit()
