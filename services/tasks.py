import random
import pandas as pd

from entities import task


def create_excel_file():
    tasks = task.get_all_tasks()
    data = []
    for row in tasks:
        data.append([row.id, row.link, row.is_exists, row.is_subscriber, row.is_posted, row.account_id])
    filename = f"{random.randint(10000, 100000)}.xlsx"
    df = pd.DataFrame(data, columns=['Id', 'Ссылка', 'Доступна ли группа', 'Подписались ли на группу',
                                     'Опубликовали ли запись',
                                     'ID связанного аккаунта'])
    df.to_excel(filename)
    return filename

