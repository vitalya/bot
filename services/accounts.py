import random
import pandas as pd

from entities import account


def create_excel_file():
    accounts = account.get_all_accounts()
    data = []
    for row in accounts:
        data.append([row.id, row.email, row.password, row.is_work, row.summary_usage_count])
    filename = f"{random.randint(10000, 100000)}.xlsx"
    df = pd.DataFrame(data, columns=['Id', 'Email', 'Password', 'Работает', 'Статистика использования'])
    df.to_excel(filename)
    return filename

