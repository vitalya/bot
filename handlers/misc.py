from aiogram import Router
from aiogram.filters import CommandStart
from aiogram.types import Message

from bot import bot
from entities import telegram as tg_db
from entities import account as acc_db
from entities import task as task_db
from keyboards import menu
from texts import misc as text

router = Router()


@router.message(CommandStart())
async def command_start_handler(message: Message) -> None:
    tg_db.add_telegram(message.chat.id, message.from_user.full_name)
    await message.answer(text.command_start_handler_text(message),
                         reply_markup=menu.main_keyboard())


@router.message()
async def handler(message: Message) -> None:
    if message.document:
        if message.document.file_name.endswith('.txt'):
            file_id = message.document.file_id
            file_path = await bot.get_file(file_id)
            file_url = file_path.file_path
            file = await bot.download_file(file_url)
            for line in file.readlines():
                line = line.strip()
                if line.count(b':') == 1 and 'https://' not in line:
                    acc_db.add_account(line.split(b':')[0].decode('utf-8'), line.split(b':')[1].decode('utf-8'))
                else:
                    task_db.add_task(line.decode('utf-8'))
            await message.reply(text.txt_uploaded())
        else:
            await message.reply(text.not_txt())
    else:
        if 'https://' in message.text:
            task_db.add_task(message.text)
            await message.reply(text.task_added())
        elif message.text.count(':') == 1:
            acc_db.add_account(message.text.split(':')[0], message.text.split(':')[1])
            await message.reply(text.account_added())
        else:
            await message.reply(text.unknown_command())
