import os

from aiogram import Router
from aiogram.filters import Command
from aiogram.types import Message, FSInputFile

from services import accounts as service
from bot import bot
from entities import account as db
from keyboards import menu
from texts import account as text

router = Router()


@router.message(lambda m: m.text == menu.accounts_btn.text)
async def accounts_handler(message: Message) -> None:
    accounts_file_path = service.create_excel_file()
    await bot.send_document(
        chat_id=message.chat.id,
        document=FSInputFile(accounts_file_path, filename="Аккаунты.xlsx"),
        caption=text.accounts_handler_text())
    os.remove(accounts_file_path)

#
# @router.message(Command("clear_accounts"))
# async def clear_accounts(message: Message) -> None:
#     db.clear_accounts()
#     await message.answer(text.clear_accounts_text())
