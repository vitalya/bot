from aiogram import Router
from aiogram.filters import Command
from aiogram.types import Message

from keyboards import menu
from entities import telegram as db
from texts import alert as text

router = Router()


@router.message(lambda m: m.text == menu.alerts_btn.text)
async def alerts_handler(message: Message) -> None:
    await message.answer(text.alerts_handler_text())


@router.message(Command("enable_posts_notification"))
async def enable_posts_notifcation(message: Message) -> None:
    db.update_tg_posts(message.chat.id, True)
    await message.answer(text.enable_posts_notification_text())


@router.message(Command("enable_messages_notification"))
async def enable_messages_notifcation(message: Message) -> None:
    db.update_tg_messages(message.chat.id, True)
    await message.answer(text.enable_messages_notifcation_text())


@router.message(Command("disable_posts_notification"))
async def disable_posts_notifcation(message: Message) -> None:
    db.update_tg_posts(message.chat.id, False)
    await message.answer(text.disable_posts_notifcation_text())


@router.message(Command("disable_messages_notification"))
async def disable_messages_notifcation(message: Message) -> None:
    db.update_tg_messages(message.chat.id, False)
    await message.answer(text.disable_messages_notifcation())
