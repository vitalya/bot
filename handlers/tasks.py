import os

from aiogram import Router
from aiogram.filters import Command
from aiogram.types import FSInputFile, Message

from bot import bot
from entities import task as db
from keyboards import menu
from services import tasks as service
from texts import task as text

router = Router()


@router.message(lambda m: m.text == menu.groups_btn.text)
async def groups_handler(message: Message) -> None:
    accounts_file_path = service.create_excel_file()
    await bot.send_document(
        chat_id=message.chat.id,
        document=FSInputFile(accounts_file_path, filename="Группы.xlsx"),
        caption=text.groups_cleared_handler_text())
    os.remove(accounts_file_path)


@router.message(Command("clear_tasks"))
async def clear_groups(message: Message) -> None:
    db.clear_tasks()
    await message.answer(text.tasks_cleared_handler_text())
