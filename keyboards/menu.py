from aiogram import types

accounts_btn = types.KeyboardButton(text="Аккаунты")
groups_btn = types.KeyboardButton(text="Задачи (группы)")
alerts_btn = types.KeyboardButton(text="Уведомления")


def main_keyboard():
    kb = [
        [accounts_btn, groups_btn],
        [alerts_btn]
    ]
    return types.ReplyKeyboardMarkup(keyboard=kb)
