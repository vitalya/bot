import asyncio
import logging
import sys
from aiogram import Dispatcher
from bot import bot
from handlers import accounts, alerts, misc, tasks

dp = Dispatcher()
dp.include_router(accounts.router)
dp.include_router(alerts.router)
dp.include_router(tasks.router)
dp.include_router(misc.router)


async def main() -> None:
    await dp.start_polling(bot)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO, stream=sys.stdout)
    asyncio.run(main())
